<div class="js-cookie-consent cookie-consent fixed bottom-0 inset-x-0 pb-2">
    <div class="max-w-7xl mx-auto px-6">
        <div class=" rounded-lg bg-yellow-100">
            <div class="flex items-center justify-between flex-wrap">
                <div class="flex-shrink-0 w-full sm:mt-0 sm:w-auto">
                    {!! trans('cookie-consent::texts.message') !!} <a class="link" href="{{ route('cookie_policy') }}">Read Cookies Policy</a>&emsp;&emsp;
                    <button class="btn btn-primary js-cookie-consent-agree cookie-consent__agree cursor-pointer flex items-center justify-center px-4 py-2 rounded-md text-sm font-medium text-yellow-800 bg-yellow-400 hover:bg-yellow-300">
                        {{ trans('cookie-consent::texts.agree') }}
                    </button>
                    <br>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>
