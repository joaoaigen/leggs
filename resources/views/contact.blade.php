@extends('layout.app')
@section('title')
    Contact
@endsection
@section('content')
    <section id="contact" class="contact mt-5">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Contact</h2>
                <p>We are here every day to help you, do not hesitate to contact us, we will attend you as soon as
                    possible, you can send us images via WhatsApp.</p>
            </div>

            <div class="row" data-aos="fade-up" data-aos-delay="100">

                <div class="col-lg-6">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="info-box">
                                <i class="bx bx-map"></i>
                                <h3>Our Address</h3>
                                <p>
                                    Workshop Unit 5<br>
                                    College Fields Business Centre<br>
                                    19 Prince Georges Road, London SW19 2PT
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-box mt-4">
                                <i class="bx bx-envelope"></i>
                                <h3>Email Us</h3>
                                <p>contact@leggsvehicles.com
                                <br><br>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-box mt-4">
                                <i class="bx bx-phone-call"></i>
                                <h3>Call Us</h3>
                                <p>
                                    <strong>Phone:</strong> +44 (0) 208 648 6555<br>
                                    <strong>WhatsApp:</strong> +44 7788 994600 <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2488.4355711701105!2d-0.17589648397899613!3d51.41342492539041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487607c49270eeb3%3A0x71f10ae24a572373!2sLeggs%20Vehicles%20Ltd!5e0!3m2!1sen!2suk!4v1645867683648!5m2!1sen!2suk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div>

        </div>
    </section>
@endsection


