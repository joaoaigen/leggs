@extends('layout.app')
@section('title')
    FAQ
@endsection
@section('content')
    <section id="faq" class="faq mt-5">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Frequently Asked Questions</h2>
            </div>

            <ul class="faq-list accordion" data-aos="fade-up">

                <li>
                    <a data-bs-toggle="collapse" class="collapsed" data-bs-target="#faq1">I have been involved in an
                        accident, can I contact you and send images? <i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq1" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            Yes you can! Send us images on our WhatsApp with every detail of the damage, we will analyse
                            and give you an estimate. But if you can come to us we can see better the situation and then
                            give you a better estimate.
                        </p>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq2" class="collapsed">Will I be charged anything in
                        addition to the estimate? <i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq2" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            No, once we give the estimate and the contract is signed, nothing more than the contract
                            will be charged or changed.
                        </p>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq3" class="collapsed">How long will it take for me
                        to get my vehicle back? <i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq3" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            It depends on each case, but we always deliver on time! This is part of the quality of our
                            service
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </section>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
@endsection


