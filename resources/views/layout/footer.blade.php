<div class="footer-top">
    <div class="container">
        <div class="row">

            <div class="col-lg-3 col-md-6 footer-contact">
                <h3>Leggs Vehicles Ltd<span></span></h3>
                <p>
                    Workshop Unit 5<br>
                    College Fields Business Centre<br>
                    19 Prince George's Road<br>
                    Colliers Wood<br>
                    London<br>
                    SW19 2PT <br><br>
                    <strong>WhatsApp:</strong> +44 7788 994600 <br>
                    <strong>Phone:</strong> +44 (0) 208 648 6555<br>
                    <strong>Email:</strong> contact@leggsvehicles.com<br>
                </p>
            </div>

            <div class="col-lg-2 col-md-6 footer-links">
                <h4>Useful Links</h4>
                <ul>
                    <li><i class="bx bx-chevron-right"></i> <a href="{{ route('home') }}">Home</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="{{ route('services') }}">Services</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="{{ route('gallery') }}">Gallery</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="{{ route('testimonials') }}">Testimonials</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="{{ route('faq') }}">FAQ</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="{{ route('contact') }}">Contact</a></li>
                    <!--<li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>-->
                    <li><i class="bx bx-chevron-right"></i> <a href="{{ route('privacy_policy') }}">Privacy policy</a></li>
                    <li><i class="bx bx-chevron-right"></i> <a href="{{ route('cookie_policy') }}">Cookies policy</a></li>
                </ul>
            </div>

            <div class="col-lg-3 col-md-6 footer-links">
                <h4>Opening Hours</h4>
                <p>Mon – Fri: <i>9am – 6pm</i></p>
                <p>Saturday: <i>9am – 1pm</i></p>
                <p>Sunday and Bank Holidays: <i>Closed</i></p>
                {{--<p>Christmas Day: <i>Closed</i></p>
                <p>Boxing Day: <i>Closed</i></p>--}}
            </div>

            <div class="col-lg-3 col-md-6 footer-links">
                <!-- TrustBox widget - Review Collector -->
                <div class="trustpilot-widget" data-locale="en-GB" data-template-id="56278e9abfbbba0bdcd568bc" data-businessunit-id="62152dd8d3a72bd339c140e9" data-style-height="52px" data-style-width="100%">
                    <a href="https://uk.trustpilot.com/review/leggsvehicles.com" target="_blank" rel="noopener">Trustpilot</a>
                </div>
                <!-- End TrustBox widget -->
            </div>

            <!--<div class="col-lg-4 col-md-6 footer-newsletter">
                <h4>Join Our Newsletter</h4>
                <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                <form action="" method="post">
                    <input type="email" name="email"><input type="submit" value="Subscribe">
                </form>
            </div>-->

        </div>
    </div>
</div>


<div class="container d-md-flex py-4">

    <div class="me-md-auto text-center text-md-start">
        <div class="copyright">
            &copy; Copyright <strong><span>Leggs Vehicles</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/presento-bootstrap-corporate-template/ -->
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
    </div>
    <div class="social-links text-center text-md-end pt-3 pt-md-0">
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="www.instagram.com/leggsvehicles/" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="https://api.whatsapp.com/send?phone=4407788994600&text=Hello%2C%20I%20come%20from%20the%20Website.%20Could%20you%20help%20me%3F"
           class="whatsap"><i class="bx bxl-whatsapp"></i></a>
    </div>
</div>
