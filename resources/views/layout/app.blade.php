<!DOCTYPE html>
<html lang="en">

<head>
    @include('layout.meta_tags')
    <title>Leggs Vehicles - @yield('title')</title>

    <!-- Favicons -->
    <link href="{{ asset('img/logo.png') }}" rel="icon">
    <link href="{{ asset('img/logo.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">
    @include('layout.css')
    @yield('style-imports')

<!-- TrustBox script -->
    <script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
    <!-- End TrustBox script -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-221017057-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-221017057-1');
    </script>

    <!-- Hotjar Tracking Code for https://leggsvehicles.com -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:2843802,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>

<body>

@include('layout.menu')
<main id="main">
    @include('cookie-consent::index')
    @yield('content')
</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer">

    @include('layout.footer')

</footer><!-- End Footer -->

<div class="row">
    {{--<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>--}}
    <a href="https://api.whatsapp.com/send?phone=4407788994600&text=Hello%2C%20I%20come%20from%20the%20Website.%20Could%20you%20help%20me%3F"
       class="move-right-whatsapp whatsapp-button d-flex align-items-center justify-content-center"
       target="_blank"><i class="bi bi-whatsapp"></i>
    </a>
</div>

@include('layout.script')
@yield('script-imports')
</body>

</html>
