<header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center border-dark border-2">
        <a href="/" class="logo me-auto"><img src="{{ asset('img/logo.png') }}" alt=""></a>

        <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>
                <li><a class="nav-link scrollto @if(\Request::segment(1) == null) active @endif" href="{{ route('home') }}">Home</a></li>
                <li><a class="nav-link scrollto @if(\Request::segment(1) == 'services') active @endif" href="{{ route('services') }}">Services</a></li>
                <li><a class="nav-link scrollto @if(\Request::segment(1) == 'gallery') active @endif" href="{{ route('gallery') }}">Gallery</a></li>
                <li><a class="nav-link scrollto @if(\Request::segment(1) == 'testimonials') active @endif" href="{{ route('testimonials') }}">Testimonials</a></li>
                <li><a class="nav-link scrollto @if(\Request::segment(1) == 'faq') active @endif" href="{{ route('faq') }}">FAQ</a></li>
                <li><a class="nav-link scrollto @if(\Request::segment(1) == 'contact') active @endif" href="{{ route('contact') }}">Contact us</a></li>
                <li><a class="nav-link scrollto" target="_blank" href="{{ url('http://leggsvehicles.portal.wsptm.com') }}">Costumer Portal</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->
    </div>
</header>
