@extends('layout.app')
@section('title')
    Services
@endsection
@section('content')
    <section id="services" class="about">
        <div class="container" data-aos="fade-up">

            <div class="row no-gutters">
                <div class="content col-xl-5 d-flex align-items-stretch">
                    <div class="content">
                        <h3>We offer all customers a first rate service</h3>
                        <p>
                            <b>Insurance claim, you have the right to choose who you trust!
                                We are here to be approved by you!</b>
                        </p>
                        <p>
                            Had enough of getting fooled with “approved insurance garages” tags.
                            We are here to ensure you will get your vehicle back on it’s best, not on the cheapest.
                        </p>
                        <ul>
                            <li>
                                Your vehicle will be back to manufacturers standards.
                            </li>
                            <li>
                                Yes, hassle free warranty on any work provided;
                            </li>
                            <li>
                                Yes, we can arrange courtesy vehicle upon request;
                            </li>
                            <li>
                                Yes, OEM parts supplied;
                            </li>
                            <li>
                                Yes, manufacturers/Thatcham methods followed;
                            </li>
                            <li>
                                Yes, colour match to manufacturers standards;
                            </li>
                            <li>
                                Yes, your safety and satisfaction is our priority.
                            </li>
                        </ul>
                        <p>
                            Fault or non-fault insurance claims, <a href="{{ route('contact') }}">contact us</a> to your options available.
                        </p>
                        <p>
                            For small dents, scratches where parts don’t need to be replaced, <a href="https://www.bodydamagequote.co.uk/#/b?subscriber=57d3f25c-797c-4652-af07-395cd6a1fd3c&liteFlow" target="_blank">click here</a> and have a rough estimate using our free estimation tool so you can have an idea of costs involved.
                        </p>
                        <p>
                            As part of our one stop shop, in partnership with qualified specialists we can also offer:
                        </p>
                        <ul>
                            <li>
                                Wheel refurbishment
                            </li>
                            <li>
                                Windscreen replacement
                            </li>
                            <li>
                                Locksmith
                            </li>
                            <li>
                                Breakdown and accident Vehicle recovery
                            </li>
                            <li>
                                Vehicle Hire
                            </li>
                            <li>
                                Vehicle Wrapping
                            </li>
                        </ul>
                        <li>
                            To achieve manufacturer standards and preserve any warranty, we replace all parts affected using manufacturers genuine parts only, following their specific repair methods and guidelines.
                        </li>
                    </div>
                </div>
                <div class="col-xl-7 d-flex align-items-stretch">
                    <div class="icon-boxes d-flex flex-column justify-content-center">
                        <div class="row">
                            <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
                                <i class="bx bxs-car-mechanic"></i>
                                <h4>Mechanical</h4>
                                <p>At Leggs Vehicles Lt, with our efficient and reliable repair and maintenance
                                    services, no matter what problem your vehicle encounters, you can trust us to leave
                                    the mechanics in perfect condition!</p>
                            </div>
                            <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
                                <i class="bx bxs-car-crash"></i>
                                <h4>Bodywork repair services</h4>
                                <p>If you’ve had a larger accident, be it your fault or somebody else’s, don’t hesitate
                                    to call in for an estimate to see if we can help. We’ll always give you our fairest
                                    price and won’t overcharge.</p>
                            </div>
                            <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
                                <i class="bx bx-spray-can"></i>
                                <h4>Paint shop</h4>
                                <p>With our paint packages, there's no better time to get your car looking like new
                                    again. At Leggs, our speciality is paint dents and scratches. The latest technology
                                    and modern equipment to restore your vehicle's paintwork and leave it perfect!</p>
                            </div>
                            <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
                                <i class="bx bx-wrench"></i>
                                <h4>Breakdown Recovery</h4>
                                <p>We have partnerships across England that provide recovery and transport to our garage with a special price!</p>
                            </div>
                        </div>
                    </div><!-- End .content-->
                </div>
            </div>

        </div>
    </section>
@endsection


