@extends('layout.app')
@section('title')
    Home
@endsection
@section('content')
    <section id="hero" class="d-flex align-items-center">

        <div class="container" data-aos="zoom-out" data-aos-delay="100">
            <div class="row">
                <div class="col-xl-6">
                    <h1>Why Leggs?</h1>
                    <h2>Outstanding Service, Outstanding you!
                        Combining the latest technology spray booth,  leading automotive paintwork materials, and highly experienced technicians, we proud ourselves on delivering  a product that is second to none, at competitive prices.
                        From a Small scuff to a full classic restoration, whatever you drive, we are here to exceed your expectations and change the concept of “Independent Repairers”.</h2>
                    <a href="https://api.whatsapp.com/send?phone=4407788994600&text=Hello%2C%20I%20come%20from%20the%20Website.%20Could%20you%20help%20me%3F"
                       target="_blank"
                       class="whatsapp scrollto"> <i class="bi bi-whatsapp"></i> Contact us</a><br><br><br><br>
                </div>
            </div>
        </div>

    </section>

    <section id="testimonials" class="testimonials">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <h2>Testimonials</h2>
                <p>We are always looking to improve our service, our environment to better receive you, and further
                    improving the quality of our work. And the results you can check here!</p>
            </div>

            <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
                <div class="swiper-wrapper">

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ url('https://lh3.googleusercontent.com/a-/AOh14GiovCgFsrP-rFHab9lr2Isfn3xh6MSldq5q6pGDjA=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img" alt="">
                                <h3>Maycon Everton Gomes</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Ótimo recomendo a todos.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ url('https://lh3.googleusercontent.com/a-/AOh14Gg0EpMHb8iAsTf_ERO-lBQrvv_OTd-gSdLWZMkr=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Avik Kar</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Such s great service with friendly staff who got the job done very quickly and were
                                    very transparent with their costs and kept me informed the whole way.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a/AATXAJwkQowx6rz6-f6AC2pCa1CQ86quK2GVcfez_57A=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Emmanuel</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    I highly recommend Leggs car repair. Staff were very friendly and helpful. Took only
                                    a couple of hours to fix a leak on my car at a reasonable price. I will definitely
                                    use their services again.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a/AATXAJyrLa44XfOJ523qCiMRuwi4c2oB2no6JYtAo6g6=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>James Horwood</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Great service from the guys at Leggs. Managed to squeeze me into their busy schedule
                                    and provided an efficient and reasonable service. Will recommend!
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14GhT8AyKBveo2gYxAuNGOTfKgr8wsWTNjWZBHfkPiwI=w60-h60-p-rp-mo-ba4-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Marineide Pina</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Very good mechanical car work...
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a/AATXAJychiquiGvooyMhSObsQTO3LDTsFlEb-D3svaYc=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Catalin Ionut</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Great service. The car looks like new after the guys touched it. Thanks
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14Gg3Wz2E8R9xW36_TTodcM9Q8SOAUSgiErichuNIHA=w60-h60-p-rp-mo-ba3-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Guilherme da Silva de Souza</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    They are the best. Kind and very good doing their job 👏🏻👏🏻👏🏻
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14Gj-neOJiGo_wm2A-QK-1de33zGVZCD4dXJ5oR1RNQ=w60-h60-p-rp-mo-ba3-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Tunay Ashimov</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    The best garage ever seen! All the guys are professional and works very clean.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a/AATXAJyCKh1SRH1KBej0YLN2wN4fyUi6rLRpOR0IYZUH=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Ricardo Rodrigues</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Great service, top costumer service.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14GgJzCKvulo-uICpEZ4xZITQnbKLTy8Mxo6jmEyF1Q=w60-h60-p-rp-mo-ba4-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Mauricio P</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Good customer service I recommend
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14GhQIRXz32GSPslNHtS0tNQh-dup_CM1PszU6eGB=w60-h60-p-rp-mo-ba3-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Tony Garcia</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    The best garage
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14Gi8czbzK379eXy7kyX74SJEvpBw1XyH13LClZN-bg=w60-h60-p-rp-mo-ba2-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Luis Abreu</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    {{--<i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    The best garage
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>--}}
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="swiper-pagination"></div>
            </div>

        </div>
    </section>

    <section id="clients" class="clients">
        <div class="container" data-aos="zoom-in">
            <div class="section-title">
                <h2>We work with all makes</h2>
            </div>
            <div class="clients-slider swiper">
                <div class="swiper-wrapper align-items-center">
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (1).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (2).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (3).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (4).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (5).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (6).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (7).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (8).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (9).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (10).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (11).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (12).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (13).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (14).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (15).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (16).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (17).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (18).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (19).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (20).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (21).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (22).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (23).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (24).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (25).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (26).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (27).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (28).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (29).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (30).png') }}"
                                                   class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('img/clients/pngwing.com (31).png') }}"
                                                   class="img-fluid" alt=""></div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>

    {{--<section id="counts" class="counts">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <h2>We work with all makes</h2>
            </div>

            <div class="row">

                <div class="col-lg-3 col-md-6">
                    <div class="count-box">
                        <i class="bi bi-emoji-smile"></i>
                        <span data-purecounter-start="0" data-purecounter-end="500" data-purecounter-duration="1"
                              class="purecounter"></span>
                        <p>Happy Clients</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
                    <div class="count-box">
                        <i class="bi bi-journal-richtext"></i>
                        <span data-purecounter-start="0" data-purecounter-end="700" data-purecounter-duration="1"
                              class="purecounter"></span>
                        <p>Projects</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
                    <div class="count-box">
                        <i class="bi bi-headset"></i>
                        <span data-purecounter-start="0" data-purecounter-end="1463" data-purecounter-duration="1"
                              class="purecounter"></span>
                        <p>Hours Of Support</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
                    <div class="count-box">
                        <i class="bi bi-people"></i>
                        <span data-purecounter-start="0" data-purecounter-end="15" data-purecounter-duration="1"
                              class="purecounter"></span>
                        <p>Hard Workers</p>
                    </div>
                </div>

            </div>

        </div>
    </section>

    <section id="tabs" class="tabs">
        <div class="container" data-aos="fade-up">

            <ul class="nav nav-tabs row d-flex">
                <li class="nav-item col-3">
                    <a class="nav-link active show" data-bs-toggle="tab" data-bs-target="#tab-1">
                        <i class="ri-gps-line"></i>
                        <h4 class="d-none d-lg-block">Modi sit est dela pireda nest</h4>
                    </a>
                </li>
                <li class="nav-item col-3">
                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#tab-2">
                        <i class="ri-body-scan-line"></i>
                        <h4 class="d-none d-lg-block">Unde praesenti mara setra le</h4>
                    </a>
                </li>
                <li class="nav-item col-3">
                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#tab-3">
                        <i class="ri-sun-line"></i>
                        <h4 class="d-none d-lg-block">Pariatur explica nitro dela</h4>
                    </a>
                </li>
                <li class="nav-item col-3">
                    <a class="nav-link" data-bs-toggle="tab" data-bs-target="#tab-4">
                        <i class="ri-store-line"></i>
                        <h4 class="d-none d-lg-block">Nostrum qui dile node</h4>
                    </a>
                </li>
            </ul>
        </div>
    </section>--}}
@endsection
