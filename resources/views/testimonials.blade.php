@extends('layout.app')
@section('title')
    Testimonials
@endsection
@section('content')
    <section id="testimonials" class="testimonials mt-5">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <h2>Testimonials</h2>
                <p>We are always looking to improve our service, our environment to better receive you, and further
                    improving the quality of our work. And the results you can check here!</p>
            </div>

            <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
                <div class="swiper-wrapper">

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ url('https://lh3.googleusercontent.com/a-/AOh14GiovCgFsrP-rFHab9lr2Isfn3xh6MSldq5q6pGDjA=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img" alt="">
                                <h3>Maycon Everton Gomes</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Ótimo recomendo a todos.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ url('https://lh3.googleusercontent.com/a-/AOh14Gg0EpMHb8iAsTf_ERO-lBQrvv_OTd-gSdLWZMkr=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Avik Kar</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Such s great service with friendly staff who got the job done very quickly and were
                                    very transparent with their costs and kept me informed the whole way.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a/AATXAJwkQowx6rz6-f6AC2pCa1CQ86quK2GVcfez_57A=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Emmanuel</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    I highly recommend Leggs car repair. Staff were very friendly and helpful. Took only
                                    a couple of hours to fix a leak on my car at a reasonable price. I will definitely
                                    use their services again.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a/AATXAJyrLa44XfOJ523qCiMRuwi4c2oB2no6JYtAo6g6=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>James Horwood</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Great service from the guys at Leggs. Managed to squeeze me into their busy schedule
                                    and provided an efficient and reasonable service. Will recommend!
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14GhT8AyKBveo2gYxAuNGOTfKgr8wsWTNjWZBHfkPiwI=w60-h60-p-rp-mo-ba4-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Marineide Pina</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Very good mechanical car work...
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a/AATXAJychiquiGvooyMhSObsQTO3LDTsFlEb-D3svaYc=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Catalin Ionut</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Great service. The car looks like new after the guys touched it. Thanks
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14Gg3Wz2E8R9xW36_TTodcM9Q8SOAUSgiErichuNIHA=w60-h60-p-rp-mo-ba3-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Guilherme da Silva de Souza</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    They are the best. Kind and very good doing their job 👏🏻👏🏻👏🏻
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14Gj-neOJiGo_wm2A-QK-1de33zGVZCD4dXJ5oR1RNQ=w60-h60-p-rp-mo-ba3-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Tunay Ashimov</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    The best garage ever seen! All the guys are professional and works very clean.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a/AATXAJyCKh1SRH1KBej0YLN2wN4fyUi6rLRpOR0IYZUH=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Ricardo Rodrigues</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Great service, top costumer service.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14GgJzCKvulo-uICpEZ4xZITQnbKLTy8Mxo6jmEyF1Q=w60-h60-p-rp-mo-ba4-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Mauricio P</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Good customer service I recommend
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14GhQIRXz32GSPslNHtS0tNQh-dup_CM1PszU6eGB=w60-h60-p-rp-mo-ba3-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Tony Garcia</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    The best garage
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14Gi8czbzK379eXy7kyX74SJEvpBw1XyH13LClZN-bg=w60-h60-p-rp-mo-ba2-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Luis Abreu</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    {{--<i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    The best garage
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>--}}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                {{--<img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14Gi8czbzK379eXy7kyX74SJEvpBw1XyH13LClZN-bg=w60-h60-p-rp-mo-ba2-br100') }}"
                                    class="testimonial-img"
                                    alt="">--}}
                                <h3>Anonymous from WhoCanFixMyCar</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Great service - friendly staff. Helped me get my car back on the road after failing it&#x27;s MOT. Gave me a very reasonable quote and then even lowered the price when they realised the job was simpler than they thought. Very honest and open. Will use again when I need to.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                {{--<img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14Gi8czbzK379eXy7kyX74SJEvpBw1XyH13LClZN-bg=w60-h60-p-rp-mo-ba2-br100') }}"
                                    class="testimonial-img"
                                    alt="">--}}
                                <h3>Anonymous from WhoCanFixMyCar</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Very happy with the work carried out. They went above and beyond to fix it. Repaired two dents on a vw golf. I can not see where they were anymore. They also cleaned up a lot of scrapes &#x2F;scratches on the bumpers. You can see the before&#x2F;after photos on the Google review I left
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                {{--<img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14Gi8czbzK379eXy7kyX74SJEvpBw1XyH13LClZN-bg=w60-h60-p-rp-mo-ba2-br100') }}"
                                    class="testimonial-img"
                                    alt="">--}}
                                <h3>Anonymous from WhoCanFixMyCar</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Great experience with Leggs. They were helpful and kept in touch. The price was very competitive, the work excellent and done on time. Thoroughly recommend.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14GgKF9pWvCSbwsOh9THEgK5l7gy1ZnFcPGChSgrUkw=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Luis Loureiro</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    {{--<i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Great experience with Leggs. They were helpful and kept in touch. The price was very competitive, the work excellent and done on time. Thoroughly recommend.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>--}}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a/AATXAJx6Cq8yQLosViIjE-sTrLhFCHFYexupS5GHC40g=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Nelson luis</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">4.8
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    {{--<i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Great experience with Leggs. They were helpful and kept in touch. The price was very competitive, the work excellent and done on time. Thoroughly recommend.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>--}}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a/AATXAJzSssTmbheUon3bqhJ3Y0YNbn-kt6VzHDF90KuQ=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Vladut MB</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    {{--<i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Great experience with Leggs. They were helpful and kept in touch. The price was very competitive, the work excellent and done on time. Thoroughly recommend.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>--}}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a/AATXAJxckdYF0Yqjw92ZcAFq11StyJhqjqbUx-qBQJfj=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Bwette Guma</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    {{--<i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Great experience with Leggs. They were helpful and kept in touch. The price was very competitive, the work excellent and done on time. Thoroughly recommend.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>--}}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <img
                                    src="{{ asset('https://lh3.googleusercontent.com/a-/AOh14Gj03Cho1Z9gPqJbTm8xFru0O8tPo589BfeWS6nTEw=w60-h60-p-rp-mo-br100') }}"
                                    class="testimonial-img"
                                    alt="">
                                <h3>Gustavo Lima Pinheiro</h3>
                                <a href="https://www.google.com/search?q=leggs+vehicles+ltd&rlz=1C1CHBF_pt-BRGB909GB909&oq=leggs&aqs=chrome.0.69i59l2j69i57j0i10l2j69i61l2j69i60.1550j0j7&sourceid=chrome&ie=UTF-8#lrd=0x487607c49270eeb3:0x71f10ae24a572373,1,,,">5.0
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                    <i class="bx bxs-star"></i>
                                </a>
                                <p>
                                    {{--<i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    Great experience with Leggs. They were helpful and kept in touch. The price was very competitive, the work excellent and done on time. Thoroughly recommend.
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>--}}
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="swiper-pagination"></div>
            </div>

        </div>
    </section>
@endsection


