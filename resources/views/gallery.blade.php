@extends('layout.app')
@section('title')
    Gallery
@endsection
@section('style-imports')
<link rel="stylesheet" type="text/css" href="{{ asset('css/twentytwenty.css') }}">
@endsection
@section('content')
    <section id="gallery" class="portfolio mt-5">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Our Gallery</h2>
                <p>Some of our work, you can check out more on our instagram page!</p>
            </div>

            <div class="row">
                <div class="col-lg-12 d-flex justify-content-center">
                    <ul id="portfolio-flters">
                        <li data-filter="*" class="filter-active">All</li>
                        <li data-filter=".filter-mercedes">Mercedes</li>
                        <li data-filter=".filter-audi">Audi</li>
                        <li data-filter=".filter-renault">Renault</li>
                        {{--<li data-filter=".filter-card">Card</li>
                        <li data-filter=".filter-web">Web</li>--}}
                    </ul>
                </div>
            </div>
            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

                <div class="col-lg-6 col-md-12 col-sm-12 portfolio-item filter-mercedes">
                        <div class="demo">
                            <img src="{{ asset('images/gallery/antes/antes-tras-lado-esquerdo.JPG') }}" />

                            <img src="{{ asset('images/gallery/tras-lado-esquerdo.JPG') }}" alt="After">
                        </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 portfolio-item filter-mercedes">
                    <div class="demo">
                        <img src="{{ asset('images/gallery/antes/antes-direita-carro-frente.JPG') }}" />

                        <img src="{{ asset('images/gallery/direito-carro.JPG') }}" alt="After">
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 portfolio-item filter-mercedes">
                    <div class="demo">
                        <img src="{{ asset('images/gallery/antes/antes-frente.JPG') }}" />

                        <img src="{{ asset('images/gallery/frente.JPG') }}" alt="After">
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 portfolio-item filter-mercedes">
                    <div class="demo">
                        <img src="{{ asset('images/gallery/antes/antes-lateral-direita-frente.JPG') }}"/>

                        <img src="{{ asset('images/gallery/lateral-direita-frente.JPG') }}" alt="After">
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 portfolio-item filter-mercedes">
                    <div class="demo">
                        <img src="{{ asset('images/gallery/antes/antes-porta-lado-esquerdo.JPG') }}" />

                        <img src="{{ asset('images/gallery/porta-lado-esquerdo.JPG') }}" alt="After">
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 portfolio-item filter-mercedes">
                    <div class="demo">
                        <img src="{{ asset('images/gallery/antes/antes-traseira.JPG') }}" />

                        <img src="{{ asset('images/gallery/parte-tras.JPG') }}" alt="After">
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 portfolio-item filter-audi">
                    <div class="demo">
                        <img src="{{ asset('images/gallery/Audi MDF/Audi-mdf-antes.JPG') }}" />

                        <img src="{{ asset('images/gallery/Audi MDF/Audi-mdf-depois.JPG') }}" alt="After">
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 portfolio-item filter-renault">
                    <div class="demo">
                        <img src="{{ asset('images/gallery/Megane/Antes.JPG') }}" />

                        <img src="{{ asset('images/gallery/Megane/Depois.JPG') }}" alt="After">
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 portfolio-item filter-audi">
                    <div class="demo">
                        <img src="{{ asset('images/gallery/Audi LZR/Antes.JPG') }}" />

                        <img src="{{ asset('images/gallery/Audi LZR/Depois.JPG') }}" alt="After">
                        <button class="btn btn-success">More informations</button>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <br>
@endsection
@section('script-imports')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="{{ asset('js/jquery.twentytwenty.js') }}"></script>
    <script src="{{ asset('js/jquery.event.move.js') }}"></script>

    <script>
        $(".demo").twentytwenty({
            //  How much of the before image is visible when the page loads
            default_offset_pct: 0.5,
            // or vertical
            orientation:'horizontal',
            // label text
            before_label:'Before',
            after_label:'After',
            // enable/disable overlay
            no_overlay:false,
            // move with handle
            move_with_handle_only:true,
            // click to move
            click_to_move:true
        });

    </script>
@endsection



