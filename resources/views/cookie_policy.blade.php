@extends('layout.app')
@section('title')
    Cookies Policy
@endsection
@section('content')
    <section id="faq" class="faq mt-5">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Cookies Policy for Leggs Vehicles</h2>
                <p>
                    This is the Cookie Policy for Leggs Vehicles, accessible from leggsvehicles.com
                </p>
            </div>
            <hr>
            <ul class="faq-list accordion" data-aos="fade-up">

                <li>
                    <a data-bs-toggle="collapse" class="collapsed" data-bs-target="#faq1">What Are Cookies?<i
                            class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq1" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            As is common practice with almost all professional websites this site uses cookies, which
                            are tiny files that are
                            downloaded to your computer, to improve your experience. This page describes what
                            information they gather, how we
                            use it and why we sometimes need to store these cookies. We will also share how you can
                            prevent these cookies from
                            being stored however this may downgrade or 'break' certain elements of the sites
                            functionality.
                        </p>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq2" class="collapsed">How We Use Cookies? <i
                            class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq2" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            We use cookies for a variety of reasons detailed below. Unfortunately in most cases there
                            are no industry standard options for disabling cookies without completely disabling the
                            functionality and features they add to this site. It is recommended that you leave on all
                            cookies if you are not sure whether you need them or not in case they are used to provide a
                            service that you use
                        </p>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq3" class="collapsed">Disabling Cookies?<i
                            class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq3" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            You can prevent the setting of cookies by adjusting the settings on your browser (see your
                            browser Help for how to do this). Be aware that disabling cookies will affect the
                            functionality of this and many other websites that you visit. Disabling cookies will usually
                            result in also disabling certain functionality and features of the this site. Therefore it
                            is recommended that you do not disable cookies.
                        </p>
                        <p>The Cookies We Set</p>
                        <br>
                        <ul>
                            <li>
                                <b>Account related cookies</b>
                                <p>If you create an account with us then we will use cookies for the management of the
                                    signup process and general administration. These cookies will usually be deleted
                                    when you log out however in some cases they may remain afterwards to remember your
                                    site preferences when logged out.</p>
                            </li>
                            <li>
                                <b>
                                    Login related cookies
                                </b>
                                <p>
                                    We use cookies when you are logged in so that we can remember this fact. This
                                    prevents you from having to log in every single time you visit a new page. These
                                    cookies are typically removed or cleared when you log out to ensure that you can
                                    only access restricted features and areas when logged in.
                                </p>
                            </li>
                            <li>
                                <b>Email newsletters related cookies</b>
                                <p>
                                    This site offers newsletter or email subscription services and cookies may be used
                                    to remember if you are already registered and whether to show certain notifications
                                    which might only be valid to subscribed/unsubscribed users.
                                </p>
                            </li>
                            <li>
                                <b>
                                    Orders processing related cookies
                                </b>
                                <p>
                                    This site offers e-commerce or payment facilities and some cookies are essential to
                                    ensure that your order is remembered between pages so that we can process it
                                    properly.
                                </p>
                            </li>
                            <li>
                                <b>
                                    Surveys related cookies
                                </b>
                                <p>From time to time we offer user surveys and questionnaires to provide you with
                                    interesting insights, helpful tools, or to understand our user base more accurately.
                                    These surveys may use cookies to remember who has already taken part in a survey or
                                    to provide you with accurate results after you change pages.</p>
                            </li>
                            <li>
                                <b>Forms related cookies</b>
                                <p>
                                    When you submit data to through a form such as those found on contact pages or
                                    comment forms cookies may be set to remember your user details for future
                                    correspondence.
                                </p>
                            </li>
                            <li>
                                <b>
                                    Site preferences cookies
                                </b>
                                In order to provide you with a great experience on this site we provide the
                                functionality to set your preferences for how this site runs when you use it. In order
                                to remember your preferences we need to set cookies so that this information can be
                                called whenever you interact with a page is affected by your preferences.
                            </li>
                        </ul>
                    </div>
                </li>


                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq4" class="collapsed">Third Party Cookies <i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq4" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            In some special cases we also use cookies provided by trusted third parties. The following section details which third party cookies you might encounter through this site.
                        </p><br>
                        <ul>
                            <li>
                                This site uses Google Analytics which is one of the most widespread and trusted analytics solution on the web for helping us to understand how you use the site and ways that we can improve your experience. These cookies may track things such as how long you spend on the site and the pages that you visit so we can continue to produce engaging content.
                                <p>
                                    For more information on Google Analytics cookies, see the official Google Analytics page.
                                </p>
                            </li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq5" class="collapsed">More Information<i
                            class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq5" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            Hopefully that has clarified things for you and as was previously mentioned if there is something that you aren't sure whether you need or not it's usually safer to leave cookies enabled in case it does interact with one of the features you use on our site.
                        </p>
                        <p>
                            However if you are still looking for more information then you can contact us through one of our preferred contact methods:
                        </p><br>
                        <ul>
                            <li>
                                Email: contact@leggsvehicles.com
                            </li>
                            <li>
                                Phone: +44 (0) 208 648 6555
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </section>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
@endsection


