@extends('layout.app')
@section('title')
    Privacy Statement
@endsection
@section('content')
    <section id="faq" class="faq mt-5">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Privacy Statement</h2>
                <p>
                    We are committed to preserving the privacy of all visitors to our website at
                    www.umcasalpelomundo.com (the “Website”). Please read the following privacy policy carefully to
                    understand how we collect, use and protect information that you provide to us, how this data
                    might be shared with our Partners/Third Parties and what choices you have in how this data may
                    be used by us. This policy also contains sections regarding how you can contact us, withdraw
                    your consent and how you can request information on what is held by us about you.
                </p><br>
                <p>
                    Important notice: If you use our Website or Services, you should read this notice to understand how we will handle your personal data.
                </p><br>
                <p>
                    This Privacy Policy should be read and any capitalised term not defined in this Privacy Policy shall have the meaning given to that term.
                </p>
            </div>
            <hr>
            <ul class="faq-list accordion" data-aos="fade-up">

                <li>
                    <a data-bs-toggle="collapse" class="collapsed" data-bs-target="#faq1">Who we are?<i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq1" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            The Website is operated by Leggs Vehicles Ltd ("We", "Us" or "LVL"). LVL is a limited company
                            registered in England and Wales under company number 11177767 and has its registered office at
                            Workshop Unit 5, College Fields Business Centre, 19 Prince George’s Road, Colliers Wood, SW19 2PT.
                            Our main trading address and principal place of business is also located at this same address.
                        </p>
                        <p>
                            If you have any questions about how we use your personal data, you can contact us on contact@leggsvehicles.com .
                        </p>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq2" class="collapsed">How We collect and receive your information? <i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq2" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            We collect information to provide our Services to all our users and also to improve our Website and Services.
                            The information We collect can range from an email address to information on how you interacted with the Website
                            and used our Services. Further details about how We collect personal data, as well as the types of personal data
                            We collect, are set out below.
                        </p><br>
                        <p>
                            We collect information in the following ways:
                        </p>
                        <ul>
                            <li><b>Account Creation</b>
                                <p>
                                    If you register to use the Website as a Private Customer you will be asked to provide certain information
                                    about yourself, including your name and contact details, comprising email address, telephone number and/or address information.
                                </p>
                            </li>
                            <li>
                                <b>Requesting a quote or an estimate</b>
                                <p>
                                    If you use the Website to request Quotes from Us, We will collect information contained in your request which
                                    will include your telephone number, car registration number, postcode and details of the work you need to be undertaken,
                                    including images of the vehicle.
                                </p>
                                <p>
                                    If you request a Quote via a third party who uses our Website or Services, your information (including car
                                    registration number, name, email address and contact details) may be stored and processed in our systems to
                                    help that third party to provide a Quote to you. The types of third parties that We work with in this respect include:
                                </p><br>
                                <ul>
                                    <li>Garage owners, Breakdown/Accident Recovery Companies and Accident Management Companies that host our
                                        booking technology on their own websites and/or social media pages.</li>

                                    <li>Parts manufacture, distribution and supply businesses that host our booking technology on their own websites and social media pages.</li>

                                    <li>Price comparison websites that use our Website and booking technology to help their customers arrange vehicle
                                        servicing and repairs or obtain Quotes for work.</li>

                                    <li>Automotive-related websites that wish to enable their visitors and customers to arrange vehicle
                                        servicing and repairs using our Website and booking technology.</li>
                                </ul>
                            </li>
                            <li>
                                <b>Information you input into the Website, or via online forms or surveys</b>
                                <p>
                                    We may also collect any other information you submit via any online forms or other interactive areas that
                                    appear on the Website from time to time.
                                </p>
                                <p>
                                    This includes information contained in any Listing, Request, Quotes or Feedback you post on the Website or
                                    which is contained in any message/chat you transmit via the Website. This could include information you supply
                                    for additional services We offer via our partners. This could include your name, contact details and information
                                    relating to the products you are enquiring into or applying for (such as date of birth, which is needed to help
                                    identify you for third party applications of credit).
                                </p>
                            </li>
                            <li>
                                <b>Telephone Calls</b>
                                <p>
                                    We collect information about telephone interaction between Customers and Partners, as well as between LVL, Mechanics, Drivers and others.
                                </p>
                                <p>
                                    This could include monitoring, recording, transcribing, storing and using communication between
                                    Customer and Partners and with LVL. It may also mean that Customers, Partners and others that call
                                    LVL offices or LVL personnel may have their contact information recorded in our systems or telecommunication
                                    devices. This includes any information you may leave in a voicemail when calling LVL offices or personnel.
                                </p>
                                <p>
                                    The reasons for collecting this information are detailed below.
                                </p>
                                <p>
                                    <span class="text-danger">PLEASE NOTE:</span> In operating as a Partner on our Website you
                                    agree that LVL may at its choosing monitor, record, transcribe, store and use communication between
                                    yourself and a Customer in order to check details of any instructions received as a result of using the Website.
                                </p>
                            </li>
                            <li>
                                <b>Operating as a Partner on the Website or using our booking technology</b>
                                <p>
                                    If you are a Partner and you purchase membership or other services on the Website or from LVL,
                                    We may collect information about your purchases as well as your credit or debit card information
                                    and We will collect information concerning the Quotes you have submitted via the Website or using our technology.
                                </p>
                            </li>
                            <li>
                                <b>Billing and other information</b>
                                <p>
                                    For Customers and Partners that purchases services from LVL including Membership packages,
                                    breakdown cover, and other paid for services provided by LVL, We may store your information with our
                                    third-party payment processors. We may store other details in third party systems to provide Us with
                                    accounting and customer relationship management abilities. This may include payment histories and contact
                                    information as well as various notes taken by LVL staff.
                                </p>
                            </li>
                            <li>
                                <b>Site usage</b>
                                <p>
                                    When you use our Website and Services We may collect information about how you interacted with our Website,
                                    including information that your browser sends when you visit a website. This log data may include your Internet Protocol address, the
                                    address of the web page you visited before using our services, and the date and time of use of our services.
                                </p>
                            </li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq3" class="collapsed">How We use your information and legal basis for doing so?<i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq3" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            Information is used in the following ways:
                        </p>
                        <p>
                            We use the information We collect from you when you use our Website or Services, to provide, maintain, protect and to improve our Website and Services, to develop new ones and to protect LVL and our users. Further information about the purposes for which we use your information and the legal basis for doing so are detailed below.
                        </p><br>
                        <ul>
                            <li>
                                <b>To provide our Website and Services and an enhanced user experience</b>
                                <p>Your information will be used to provide you with access to our Website and to supply
                                    you with Services you request. In particular:</p>
                                <p>If you are a Customer and request quotes on our Website, which services are
                                    provided by our trusted partners, We will use your postcode to select trusted
                                    partners who are best placed to respond and We will send details of the request,
                                    together with your first name, to these selected partners so they can respond. The car
                                    registration number will be provided to the partners and may be shared to help identify
                                    the make and model of the vehicle, and help provide services to our partners in relation
                                    to parts data, parts information, recovery so that the partners can provide you a quote
                                    on your service request.
                                </p>
                                <p>Any partners who respond to your request will also be given access to your telephone number
                                    so that they can contact you to discuss your requirements and their quotation. If you select
                                    a partner to undertake the job via the Website, then your chosen partner will be given access
                                    to your other contact details so that they can contact you to arrange for the work to be undertaken.
                                    Some partners use third party suppliers and agencies to help them to respond to requests for Quotes and
                                    to liaise with customers to arrange for work to be undertaken and so references to partners in this
                                    privacy policy includes such third parties acting on their behalf.
                                </p>
                                <p>
                                    Please see how to manage your telephone communication with partners below.
                                </p>
                                <p>
                                    The Website allows the partners to communicate via messaging/chat to Customers once a Job is quoted on,
                                    and likewise a Customer can communicate via message/chat to a Partner. This functionality allows you to
                                    ask questions and transmit information between each other that might be useful to either party in assessing
                                    and understanding a quote. The chat system allows this flow of information to be exchanged in a controlled
                                    manner. However, if you provide personal contact or other personal information,  DO NOT AT ANY POINT SHARE
                                    YOUR CARD DETAILS IN ANY WAY either over the phone, chat or images, if you do so, you do at your own risk.
                                    LVL may also review the messaging/chat system from time to time, and may use any or all of the information
                                    for various purposes including assisting us in contacting you, assisting us in any legal proceedings or dispute,
                                    assisting us in our analysis of completed bookings, assisting us in the development of various enhancements of
                                    our systems or Website or in any other manner that we may be required under law to provide such information.
                                </p>
                                <p>
                                    Legal Basis:
                                </p>
                                <p>
                                    This is often necessary in order to be able to fulfil our obligations under the contract between us.
                                </p>
                                <p>
                                    In other cases this is necessary for our legitimate interests (including managing our relationship
                                    with Customers and Partners, enforcing our contractual rights and resolving disputes, keeping records,
                                    studying how customers use our Website and Services and improving our Website and Services).
                                </p>
                            </li>
                            <li>
                                <b>
                                    To communicate with you by Email
                                </b>
                                <p>
                                    We send a variety of emails to you, both as Customers and Partners and as other consumers of services
                                    in our Website. These include other services you may have subscribed to or purchased, such as breakdown cover, or repair finance.
                                </p>
                                <p>
                                    Some of the emails We send you are part of the functionality of how the Website and our Services work,
                                    for instance We inform you if you have received a Quote if you’re a Customer. If you are registered
                                    as a Partner, We may send you renewal notices etc. We categorise emails of this nature as "transactional
                                    emails". They are intended to give you information in relation to the requests you are making in the system
                                    and prompt a response from you to those requests.
                                </p>
                                <p>
                                    Other types of emails that We may send you may be marketing-related emails. If you register with us as a
                                    Customer or Partner or via another service we offer (breakdown cover etc.) and have consented to receive
                                    such information, We may use your details to provide you with, or permit selected third parties to provide
                                    you with, information about goods, services and offers that We feel may interest you.
                                </p>
                                <p>
                                    For details on how to change your consent to receive these emails, please contact us on contact@leggsvehicles.com.
                                </p>
                                <p>
                                    The other type of emails we may send to you is regarding interaction with LVL staff and may result in
                                    our requesting more information from you about certain matters, or providing you with an update on your
                                    account etc.
                                </p>
                                <p>
                                    Legal Basis:
                                </p>
                                <p>
                                    In the case of transactional emails, this is necessary in order to be able to fulfil our
                                    obligations under the contract between us.
                                </p>
                                <p>
                                    Transactional emails (and emails responding to queries or requesting information) are also necessary for our
                                    legitimate interests (including to manage our relationship with you and to resolve queries and disputes).
                                </p>
                                <p>
                                    Customers and Partners who registered to use our Website and Service will be asked if they want to opt in to receive
                                    marketing emails and so we will only send marketing emails to these customers if they have consented to receive them.
                                </p>
                            </li>
                            <li>
                                <b>To communicate with you by SMS</b>
                                <p>
                                    We send a variety of SMS to you, both as Customers and Partners and as other consumers of services in our Website.
                                    These include other services you may have subscribed to or purchased, such as breakdown cover, or repair finance.
                                </p>
                                <p>
                                    Some of the SMS We send you are part of the functionality of how the Website and our Services work, for instance
                                    We may send you reminders about your booking, or ask you to confirm if you have had the work done, if you’re a Customer.
                                    If you are registered as a Partner, We may send SMS relating to messages you have received from a Customer. We categorise
                                    SMS of this nature as "transactional SMS". They are intended to give you information in relation to the requests you are
                                    making in the system and prompt a response from you to those requests.
                                </p>
                                <p>
                                    Other types of SMS that We may send you may be marketing-related SMS. If you register with us as a Driver or Mechanic
                                    or via another service we offer (breakdown cover etc.) and have consented to receive such information, We may use your
                                    details to provide you with, or permit selected third parties to provide you with, information about goods, services and
                                    offers that We feel may interest you.
                                </p>
                                <p>
                                    For details on how to change your consent to receive these marketing related SMS and emails, refer to the Your
                                    Rights and Choices section of this policy.
                                </p>
                                <p>
                                    The other type of SMS’s we may send to you is regarding interaction with WCFMC staff and may result in our
                                    requesting more information from you about certain matters, or providing you with an update on your account etc.
                                </p>
                                <p>
                                    Legal Basis:
                                </p>
                                <p>
                                    In the case of transactional SMS, this is necessary in order to be able to fulfil our obligations under the contract between us.
                                </p>
                                <p>
                                    Transactional SMS (and SMS responding to queries or requesting information) are also necessary for our legitimate interests
                                    (including to manage our relationship with you and to resolve queries and disputes).
                                </p>
                                <p>
                                    Customers and Partners who registered to use our Website and Service will be asked if they want to opt in to receive marketing
                                    SMS and so we will only send marketing SMS to these customers if they have consented to receive them.
                                </p>
                            </li>
                            <li>
                                <b>
                                    To enable our users to communicate with us and each other by Telephone
                                </b>
                                <p>
                                    As disclosed above, Partners who respond to a request for a Quote will also be given access to the telephone number a Customer
                                    provided so that they can contact the Customer to discuss their requirements and their quotation. Likewise, Customers are
                                    able to access Partners telephone numbers in order to call them and enquire about the quotes they have received.
                                </p>
                                <p>
                                    Please see the section below on how Customers can manage your telephone communication with Partners.
                                </p>
                                <p>
                                    Drivers may also be called in connection to other services they actively purchase from LVL. These calls will be for functional
                                    purposes related to those purchases, such as liaising with the Customer on their breakdown, or service plan booking.
                                </p>
                                <p>
                                    LVL staff may also use both the Partner and/or Customer telephone numbers to transfer calls to either party, to assist
                                    us in our analysis of completed bookings, to make enquiries on behalf of Customers to Partners and to assist us in any
                                    dispute investigations We may conduct. Additionally, We will use Partner telephone numbers to conduct account management
                                    calls, sales calls, and other calls to Partners on behalf of LVL or our partners.
                                </p>
                                <p>
                                    LVL may monitor, record, transcribe, store and use communication between Customers and Partners and their communications
                                    with Us. We will only use this information for the following:
                                </p>
                            </li>
                            <ul>
                                <li>
                                    to help us in our analysis of completed bookings;
                                </li>
                                <li>
                                    for training purposes; and
                                </li>
                                <li>
                                    for legal reasons, including regulatory requirements.
                                </li>
                            </ul>
                            <p>
                                If you register as a Partner with Us you are acknowledging and accepting the following:
                            </p><br>
                            <ul>
                                <li>
                                    you are only being given the access to Customer telephone numbers for the purposes
                                    of responding to their enquiry on our Website and not for any other reasons;
                                </li>
                                <li>
                                    you are registering with us as a business and as such you will maintain business-like
                                    conduct with our staff and with Customers who you contact, or who contact you through LVL;
                                </li>
                                <li>
                                    you agree to have your telephone number used in connection to marketing and sales-related activities from LVL to your business; and
                                </li>
                                <li>
                                    you understand that LVL may at its choosing monitor, record, transcribe, store and use communication between yourself and a Customer in order to check details of any instructions received as a result of using the Website.
                                </li>
                            </ul>
                            <p>
                                Legal Basis:
                            </p>
                            <p>
                                In some cases this is necessary in order to fulfil our obligations under our contract with you.
                            </p>
                            <p>
                                In other cases this is necessary for our legitimate interests (including managing our relationship with Drivers and Mechanics,
                                enforcing our contractual rights and resolving disputes, keeping records,
                                studying how customers use our Website and Services, improving our Website and Services and growing our business).
                            </p>
                            <p>
                                In some cases this may also be necessary to ensure compliance without legal obligation.
                            </p><br>
                            <ul>
                                <li>
                                    For Feedback purposes
                                    <p>
                                        If you are a Customer and post Feedback, the content of the Feedback will be published on our Website. Partners will also have the
                                        right to post replies to any Feedback posted and the content will be published on our Website. All Feedback will be posted at the
                                        discretion of LVL.
                                    </p>
                                    <p>
                                        If you register with Us We may use your information to contact you for feedback on your use or our products, services or the Website.
                                    </p>
                                    <p>
                                        Legal Basis:
                                    </p>
                                    <p>
                                        This is necessary for our legitimate interests (including studying how our customers use our Website and Services, monitoring customer
                                        performance in the case of Partners and customer satisfaction in the case of Customers and developing and improving our Website and Services).
                                    </p>
                                </li>
                                <li>
                                    To manage Partners Listing Information and other information you provide Us as a Partner
                                    <p>
                                        If you are a Partner, We will publish the details of any Listing you create on the Website. As a Partner you
                                        should not include any contact details for your business within your public profile on the Website, as this
                                        will be published at the discretion of LVL.
                                    </p>
                                    <p>
                                        As a Partner you may be asked to provide supporting information in relation to your business and identity. We will only use this
                                        information in relation our internal approval process, and for any products you are wishing to purchase from/or via our partners,
                                        such as motor trade insurance. We may also use any additional information you provide us for any legal or regulatory obligations
                                        We may be under in connection to your business and the services We provide.
                                    </p>
                                    <p>
                                        We may use any Quotes you provide Us or the Customer in a manner consistent with the Website’s Intellectual Property rights.
                                        This could include, but not limited to, analysis of such pricing on a repair/service category on a regional, national level,
                                        or by type of business that provided the Quotes.
                                    </p>
                                    <p>
                                        Legal Basis:
                                    </p>
                                    <p>
                                        In some cases this is necessary in order to fulfil our obligations under our contract with you.
                                    </p>
                                    <p>
                                        In other cases, this is necessary for our legitimate interests (including managing our relationship with you, enforcing our
                                        contractual rights and resolving disputes, keeping records, improving our Website and Services and growing our business).
                                    </p>
                                    <p>
                                        In some cases this may also be necessary to ensure compliance with our legal obligations.
                                    </p>
                                </li>
                                <li>
                                    For Billing and Account Management purposes
                                    <p>
                                        We use your data to help administer your accounts, both as a Customer and Partner.
                                    </p>
                                    <p>
                                        As a Partner We will use your information to inform you about Jobs which have been posted on our Website by Customers
                                        and if you post a Quote for a Job this will be shared with the Customer concerned. We will also use information concerning
                                        the Quotes you have submitted and your use of the Website for billing purposes and to ensure that you are complying with your obligations.
                                    </p>
                                    <p>
                                        As a Customer we will use your information to process payments for additional services you may purchase from
                                        LVL or our partners that use LVL systems to provide you with services you have purchased.
                                    </p>
                                    <p>
                                        As a Customer your car registration number may be used in conjunction with Partner, and other information to identify jobs
                                        that may have been completed. In addition, your car registration number may be shared with our Third Party partners to help us
                                        identify parts that may have been used in any repair or service for billing purposes. Your car registration may also be contained
                                        in reports to Partners to help them identify work being sent to them, quoted on by the, and completed by them.
                                    </p>
                                    <p>
                                        Legal Basis:
                                    </p>
                                    <p>
                                        This is necessary in order to fulfil our obligations under our contract with you.
                                    </p>
                                    <p>
                                        This is also necessary for our legitimate interests (including managing our relationship with you, enforcing our contractual
                                        rights and resolving disputes and keeping records).
                                    </p>
                                </li>
                                <li>
                                    Website Usage Analysis
                                    <p>
                                        Legal Basis:
                                    </p>
                                    <p>
                                        This is necessary for our legitimate interests (including studying how customers use our Website and Services, improving our
                                        Website and Services and growing our business).
                                    </p>
                                    <p>
                                        We may use and analyse the information We collect so that We can administer, support, improve, optimise and develop
                                        the Website and the products and services We offer. This information could be related to your activity on the Website,
                                        types of work listed, quotes, regional data, car make/model data and other patterns that are collected from both Jobs being
                                        posted by Customers and Quotes provided by Partners.
                                    </p>
                                </li>
                                <li>
                                    Marketing
                                    <p>
                                        We may also share certain data with third party social media platforms in order to show you targeted ads when you visit them. We do this by:
                                    </p>
                                </li>
                                <li>
                                    the use of cookies which capture your visits to our Website. Please refer to our Cookies Policy for more information; and
                                </li>
                                <li>
                                    We may also provide these platforms with your email address to create ‘audiences’ of users fitting within a certain demographic/category
                                    so that we can target our marketing. Please check the social media platforms’ terms for more details of these services. Our Cookies Policy
                                    explains how you can adjust your cookies preferences.
                                </li>
                            </ul>
                            <p>
                                Legal Basis:
                            </p>
                            <p>
                                This is necessary for our legitimate interests (including studying how customers use our Website and Services, improving our Website and
                                Services and growing our business through marketing to you or making you offers that we believe will interest you).
                            </p><br>
                            <ul>
                                <li>
                                    Other ways we may use your data
                                    <p>
                                        We may use your data to investigate, resolve and transfer complaints between parties.
                                    </p>
                                    <p>
                                        We may receive, requests from official government/local government bodies and law enforcement authorities and to help Us identify and fix issues with our systems.
                                    </p>
                                    <p>
                                        Legal Basis:
                                    </p>
                                    <p>
                                        This is necessary for our legitimate interests (including resolving complaints and disputes, maintaining and improving our systems).
                                    </p>
                                    <p>
                                        In some cases this may be necessary to comply with legal obligations which we are subject to.
                                    </p>
                                </li>
                                <li>
                                    PLEASE NOTE: This policy is not intended to place any limits on what We do with data that is aggregated and de-identified, so
                                    it is no longer associated with an identifiable Customer or Partner.
                                </li>
                                <li>
                                    PLEASE NOTE: As we indicate above, some processing of your personal data may be necessary to fulfil our contractual obligations to you. This includes processing
                                    your information to provide Services to you. If you do not provide this information, we may not be able to fulfil our contractual obligations to you.
                                </li>
                            </ul>
                        </ul>
                    </div>
                </li>


                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq4" class="collapsed">How your information may be shared and with whom? <i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq4" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            Our employees who need to have access to your personal data will have access to it in order to provide Services to you and for
                            the purposes detailed above. We will also share your information with other users of the Website and Services in the ways described
                            in Section 2 (How We Use Your Information). We may also share your information with other third parties in the ways described below.
                        </p>
                        <p>
                            Third parties for marketing purposes
                        </p>
                        <p>
                            If you have consented to receiving marketing information from selected third parties, We may share your information with third parties so that
                            they can provide you with information about the products and services they offer.
                        </p>
                        <p>
                            We may also share certain data with third party social media platforms in order to show you targeted ads when you visit them. We do this by:
                        </p><br>
                        <ul>
                            <li>
                                The use of cookies which capture your visits to our Website. Please refer to our Cookies Policy for more information; and
                                <a href="">Link for Cookies Policy</a>
                            </li>
                            <li>
                                We may also provide these platforms with your email address to create ‘audiences’ of users fitting within a certain demographic/category
                                so that we can target our marketing. Please check the social media platforms’ terms for more details of these services.
                                This is in our legitimate interests of sending you direct marketing. Our Cookies Policy explains how you can adjust your cookies preferences.
                            </li>
                            <p>
                                We may allow third-party providers and contractors who supply services to Us or who process information on our behalf
                                (for example our payment provider and website hosts and third parties who help us to develop and support our Website)
                                access to your information limited to the extent that it is necessary for them to perform their services. We will make
                                sure we have a contract in place with all such suppliers which satisfies the requirements of data protection laws. In
                                particular, we will ensure that they keep your information secure and do not use it for their own purposes, unless you
                                grant them permission to do so.
                            </p><br>
                            <li>
                                During changes to our business structure
                                <p>
                                    We may share your information with another organisation which buys our business or our assets or to whom We transfer our agreement
                                    with you and in the course of any negotiations which may or may not lead to such a transfer or sale.
                                </p>
                            </li>

                            <li>
                                To comply with laws
                                <p>
                                    We will disclose your information if We are required to by law. We may disclose your information to enforcement
                                    authorities if they ask us to, or to a third party in the context of actual or threatened legal proceedings,
                                    provided We can do so without breaching data protection laws
                                </p>
                            </li>

                            <li>
                                In working with partners to offer enhanced or further services, and to help manage our accounts
                                <p>
                                    If you are a Partner registered with Us and you have expressed your interest joining one of the various networks that
                                    We are deploying, then your contact information may be passed to the suitable network partner to help onboard you to that network.
                                </p>
                                <p>
                                    As a Customer, your car registration number may be shared with our partners in order to.
                                </p>
                            </li>
                            <li>
                                Help identify parts and other specific information related to your car make and model.
                            </li>
                            <li>
                                Help identify jobs that Mechanics may be completed by purchasing parts related to your car registration number.
                            </li>
                            <li>
                                Help provide reporting to those partners on what work is being done via our Website.
                            </li>
                            <li>
                                Working with third party referrers of visitors to our website and third-party suppliers to our Partners
                                <p>
                                    If you have visited the Website as a result of a referral link or integration with a third-party site
                                    with which We hold a commercial relationship, such as another automotive website or price comparison
                                    platform, We may share selected elements of your information with that party, such as your vehicle
                                    registration number, during the process of regular reporting and verification of our commercial
                                    relationships with them. We will only share this information to the extent that it is required to
                                    confirm that relationship and only with the partners with which a relationship has been tracked by our system.
                                </p>
                                <p>
                                    Partners may choose to take advantage of commercial partnerships We hold with suppliers of vehicle parts, or businesses
                                    of a similar nature, to procure supplies for the completion of the servicing or repair of your vehicle. We may share
                                    selected elements of your information with those partners, such as your vehicle registration number, during the process
                                    of regular reporting and verification of our commercial relationships with those partners. We will only share this
                                    information to the extent that it is required to confirm that relationship and transaction.
                                </p>
                            </li>
                            <li>
                                To enforce our rights, prevent fraud, and for safety
                                <p>
                                    To protect and defend the rights, property, or safety of LVL or third parties, including enforcing contracts or policies,
                                    or in connection with investigating and preventing fraud.
                                </p>
                            </li>
                            <li>
                                PLEASE NOTE: In registering as a Partner, you agree to the Websites Terms and Conditions and Code of Conduct. In line with
                                this, LVL may disclose your contact details to Customers (if requested by the Customer) in the event disputes arise between
                                you and the Customer.

                                <p>
                                    PLEASE NOTE: We may disclose or use aggregated and de-identified information for any purpose. For example, we may share
                                    aggregated and de-identified information with our partners or others for business or research purposes like telling a
                                    prospective Partner the number of Jobs available in a certain area, or partnering with a publisher to detail cost of
                                    servicing by make and model of car to its readers.
                                </p>
                            </li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq5" class="collapsed">Cookie Policy<i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq5" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            LVL uses cookies and similar technologies to provide and support the Website and services.
                        </p>
                        <p>
                            Cookies are small text files which are transferred from the Website and stored on your computer's hard drive.
                            They enable the Website and analytical software to assign stateful information to visitors at an individual level.
                            Please refer to our Cookies Policy for details of the Cookies use on the Website.
                        </p>
                        <p>
                            By registering to use the Website you are consenting to Us using cookies in the ways described in our Cookies policy.
                        </p>
                        <p>
                            You may be able to configure your browser to restrict cookies or block all cookies if you wish. However, if you disable
                            cookies you may find this adversely affects your ability to use certain parts of this Website
                        </p>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq6" class="collapsed">Our responsibilities<i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq6" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            We are responsible for all use We make of the information you provide to Us and for ensuring that We only use and disclose your
                            information in accordance with the terms of this Privacy Policy and applicable laws.
                        </p>
                        <p>
                            If you are a Customer and request Partners to provide Quotes or undertake work for you, you understand and agree that those
                            Partners (and not LVL) are solely responsible for their use of your information. Whilst We ask all Partners to act responsibly
                            and to comply with all applicable data protection and privacy laws We cannot accept any responsibility for their use of your
                            information. If you are a Customer and have questions or concerns about any Partner’s use of your information, please raise
                            the matter with the Partner in question in the first instance.
                        </p>
                        <p>
                            Similarly, if you are a Customer or a Partner and you enter into a separate contract or arrangement with one of our partner
                            organisations or another third party (e.g. to buy products or services directly from them) then you understand that
                            they will be solely responsible for their use of your information in the context of or as a result of that contract or
                            relationship. If you have any questions about how they will use your personal data, please raise them with the partner
                            organisation or third party in the first instance or review their privacy policy or notices.
                        </p>
                        <p>
                            If you would like to then raise your concern to WCFMC please contact Us at contact@leggsvehicles.com
                        </p>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq7" class="collapsed">Your rights and choices<i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq7" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            We have to hold certain personal data about you, e.g. email addresses and telephone numbers, etc. However, we provide you with certain
                            choices about how your personal data can be used. We will also operate in accordance with data protection laws in maintaining, protecting,
                            and using your personal data and we will respect your rights under data protection laws.
                        </p>
                        <p>
                            Your Choices:
                        </p>
                        <p>
                            We give you the following choices when you use our Website and Services:
                        </p>
                        <p>
                            Opt-in/out of receiving marketing emails: You have the choice to opt-in or out of receiving any marketing email from Us and any third party at any time.
                        </p>
                        <p>
                            Block Cookies: You have the choice on how your browser or mobile phone works with the Website. You may set your browser to block all cookies
                            associated with our services. Please see our Cookie Policy for more information about this. However, it is important to note that the
                            Website may not function properly if some or all of the cookies We use are disabled. For instance, how you can gain access to your account
                            regularly.
                        </p>
                        <p>
                            Customers choices about use of the telephone number: If you are a Customer and wish to remove your telephone number from the system,
                            you can email Us at contact@leggsvehicles.com and We can remove your active number from our system records. We may ask you for supporting
                            evidence of your identity, before We remove the telephone number from our main system. Note: This may make it difficult for Partners to quote
                            correctly on your job and they may request your telephone number via our messaging/chat systems. If you put contact details into any message
                            you send the Partners, We may not be able to prevent the Partner from holding your details in their own records. Any personal information
                            you supply in our messaging/chat system you do at your own risk.
                        </p>
                        <p>
                            Your Rights under data protection law:
                        </p>
                        <p>
                            Under data protection legislation you also have certain rights regarding your personal data. In particular, you have the right to:
                        </p><br>
                        <ul>
                            <li>
                                Request access to any personal data we hold about you. In order to do this, you can submit a Subject Access Request Form to Us by post,
                                You can request the form and the supporting evidence you will need to provide to prove your identity please email contact@leggsvehicles.com;
                            </li>

                            <li>
                                Have any personal data which we hold about you which is inaccurate rectified or to have incomplete personal data completed.
                                You can do that via updating your profile information, or requesting that changes be made in our systems via email.
                                We might ask you to submit supporting evidence of your identity before We comply with any request.
                            </li>
                            <li>
                                Have personal data erased;
                            </li>
                            <li>
                                Have the processing of your personal data restricted (for example, if you think the data we hold about you is inaccurate you
                                can ask us to stop processing it, until we will either correct it or confirm it is accurate);
                            </li>
                            <li>
                                Be provided with the personal data that you have supplied to us, in a portable format that can be transmitted to another
                                data controller without difficulty;
                            </li>
                            <li>
                                Object to certain types of processing, including processing based on legitimate interests (see above), automated processing
                                (which includes profiling) and processing for direct-marketing purposes; and
                            </li>
                            <li>
                                Not be subject to a decision that is based solely on automated processing which produces a legal effect or which has a
                                similar significant effect for you.
                            </li>
                        </ul>
                        <p>
                            For more information on what rights you have please see the Information Commissioners website at <a class="link-info" href="http://www.ico.gov.uk">http://www.ico.gov.uk.</a>
                        </p>
                        <p>
                            In order to exercise any of these rights you can to submit a request to us via email to contact@leggsvehicles.com.  We might ask you to submit supporting evidence of your identity before We comply with any request.
                        </p>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq8" class="collapsed">Security and Retention of information <i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq8" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            We will keep your information secure by taking appropriate technical and organisational measures against its unauthorised or
                            unlawful processing and against its accidental loss, destruction or damage. We will retain your information for as long as
                            We need it for the purposes set out in this policy, or as otherwise required by law. This means that personal data will be
                            destroyed or erased from our systems when it is no longer required. For more information on how long certain types of data
                            is likely to the kept before being destroyed please contact our Data Protection Manager.
                        </p>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq9" class="collapsed">Changes to privacy policy<i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq9" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            We will from time to time update and make changes to this policy. We recommend that you revisit this privacy policy frequently,
                            and ideally each time you visit the Website. If you continue to use our services after those changes are in effect, you agree to
                            the revised policy.
                        </p>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq10" class="collapsed">Links from our website to others<i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq10" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            If you follow a link from any part of the Website to another site, this policy will no longer apply. We are not responsible
                            for the information handling practices of third party sites and We encourage you to read the privacy policies appearing on those sites.
                        </p>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq11" class="collapsed">Transfers of Data Outside the European Economic Area (EEA)<i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq11" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            We will not transfer your personal data outside the EEA unless such transfer is compliant with data protection laws. This means that we cannot transfer any of your personal data outside the EEA unless:
                        </p><br>
                        <ul>
                            <li>
                                The EU Commission has decided that another country or international organisation ensures an adequate level of protection for your personal data;
                            </li>
                            <li>
                                The transfer of your personal data is subject to appropriate safeguards, which may include binding corporate rules or standard data protection clauses adopted by the EU Commission or EU-US Privacy Shield framework (see https://www.privacyshield.gov; or
                            </li>
                            <li>
                                One of the derogations under data protection laws applies (including if you explicitly consent to the proposed transfer).
                            </li>
                        </ul>
                        <p>
                            We currently use a number of suppliers of IT services (including cloud- based services, hosting services and software development,
                            support and maintenance services) who are based outside the EEA. All such suppliers have either entered into an agreement with us
                            containing EU Commission approved data protection clauses, or they are signed up the EU/US Privacy Shield framework.
                        </p>
                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq12" class="collapsed">How to Contact Us<i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq12" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            Feel free to contact Us if you have any questions about this policy or if you wish to exercise your
                            information rights about any personal data you have shared with Us, or that we might hold.
                        </p>
                        <p>
                            For general enquires you can contact by email at contact@leggsvehicles.com
                        </p>
                        <p>If you wish to contact Us in regards to a complaint please email us at contact@leggsvehicles.com</p>

                        <p>If you wish to contact Us in regard to exercising your information rights please contact us at contact@leggsvehicles.com</p>

                        <p>You can also write to Us at</p>
                        <p>
                            Leggs Vehicles Ltd, Workshop Unit 5, College Fields Business Centre, 19 Prince George’s Road, Colliers Wood, SW19 2PT.
                        </p>

                    </div>
                </li>

                <li>
                    <a data-bs-toggle="collapse" data-bs-target="#faq13" class="collapsed">Right to make a complaint<i class="bx bx-chevron-down icon-show"></i><i
                            class="bx bx-x icon-close"></i></a>
                    <div id="faq13" class="collapse" data-bs-parent=".faq-list">
                        <p>
                            If you have any issues with our processing of your personal data please contact us in the first instance and
                            we will endeavour to resolve it for you.
                        </p>
                        <p>
                            This Privacy Policy was last updated on 20 January 2022
                        </p>
                    </div>
                </li>


            </ul>
        </div>
    </section>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
@endsection


